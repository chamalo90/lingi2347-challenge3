package com.jcolmonts.challenge3;

import java.io.IOException;

import com.jcolmonts.challenge3.model.RainbowTable;

public class Precompute {

	public static void precompute(){
		RainbowTable rt = new RainbowTable(Challenge3.CHAINLENGTH, Challenge3.CHARSET, Challenge3.LINES_NUMBER);
		try {
			rt.generate();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
