package com.jcolmonts.challenge3;


public class Challenge3 {
	public static final int CHAINLENGTH = 8000;
	public static final int LINES_NUMBER = 500000;
	public static final String CHARSET = "0123456789";
	
	public static void main(String[] args){
		System.out.println("---------------------------------------------");
		System.out.println("Challenge 3 Solver - Julien Colmonts");
		System.out.println("---------------------------------------------");
		if (args.length < 1) {
			System.out.println("Parameters : ");
			System.out.println("	'precompute' : precompute hashes");
			System.out
					.println("	'find hashfile' : try to find passwords related with hashes contained in hashfile");
			System.out.println("---------------------------------------------");
			return;
		}
		if(args[0].equals("precompute")){
			Precompute.precompute();
			return;
		}else if(args[0].equals("find")){
			if(args.length<2){
				System.out.println("No hashfile given as parameter - execute with no parameter for help");
				return;
			}
			String hashFile = args[1];
			Find.find(hashFile);
			
		}else{
			System.out.println("Parameters : ");
			System.out.println("	'precompute' : precompute hashes");
			System.out.println("	'find hashfile' : try to find passwords related with hashes contained in hashfile");
			System.out.println("---------------------------------------------");
			
		}
	}
}
