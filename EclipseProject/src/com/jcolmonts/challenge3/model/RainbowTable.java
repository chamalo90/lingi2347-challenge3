package com.jcolmonts.challenge3.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import com.jcolmonts.challenge3.Challenge3;
import com.jcolmonts.challenge3.HexUtils;

/**
 * Object representation of RainbowTable
 * 
 * @author julien
 * 
 */
public class RainbowTable {

	// Static variables
	public static final String HASH_FILE_NAME = "jcolmonts_rainbow_table";
	public static final String CRACKED_FILE_NAME = "cracked_hashes";
	public static final BigInteger MAXIMUM_INTEGER = new BigInteger(
			"4294967295");

	public int chainLength;
	public String characterSet;
	public int lines;
	public List<RainbowTableLine> rl;

	public RainbowTable(int chainLength, String characterSet, int lines) {
		this.chainLength = chainLength;
		this.characterSet = characterSet;
		this.lines = lines;
	}

	public static BigInteger reduceFunction(byte[] hash, int columnIndex) {
		BigInteger hashElem = new BigInteger(1, hash);
		hashElem.add(BigInteger.valueOf(columnIndex));
		hashElem = hashElem.mod(MAXIMUM_INTEGER);
		if (hashElem.bitCount() % 2 != 1) {
			if (hashElem.bitLength() == 0) {
				hashElem = hashElem.flipBit(columnIndex % 1);
			} else {
				hashElem = hashElem.flipBit(columnIndex % hashElem.bitLength());
			}

		}
		return hashElem;
	}

	public static byte[] tripleSha256Encryption(BigInteger bg) {
		ByteBuffer b = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder());
		byte[] byteArray = bg.toByteArray();
		for (int i = 0; i < byteArray.length && i < 4; i++) {
			b.put(i, byteArray[byteArray.length - 1 - i]);
		}
		return sha256(sha256(sha256(b.array())));

	}

	public void generate() throws IOException{

		ArrayList<BigInteger> finalHashes = new ArrayList<BigInteger>();
		BufferedWriter writer = new BufferedWriter(new FileWriter(
				HASH_FILE_NAME, true));

		System.out.println("---------------------------------------------");
		System.out.println("Starting Rainbow Table Computation ... ... ...");
		System.out.println("---------------------------------------------");


		for(int counter = 0; counter<this.lines; counter++){

			BigInteger bg = generateRandomNumber();

			if(counter%1000==0){

				int percent = (int) (((float) counter/Challenge3.LINES_NUMBER)*100);

				System.out.println("Finished " + percent+ "%"  );
			}

			BigInteger finalHash = computeFinalHash(bg,chainLength);
			boolean compare = false;

			for(BigInteger cmp:finalHashes){
				if(cmp.equals(finalHash)){
					compare=true;
					counter--;
					break;
				}
			}
			if(!compare){
				finalHashes.add(finalHash);
				writer.write((new RainbowTableLine(finalHash, bg.toString(16)))
						.toString());
				writer.newLine();

			}
		}

		writer.close();
		System.out.println("---------------------------------------------");
		System.out.println("Finished !");
		System.out.println("---------------------------------------------");

	}

	public void crack(String hash) {
	
	
		for(int i=this.chainLength;i>-1;i--){
			byte[]finalHash = HexUtils.fromHexString(hash);
			BigInteger bg;
			for(int j=i; j<this.chainLength;j++){
				bg = reduceFunction(finalHash, j);
				finalHash = tripleSha256Encryption(bg);
			}
	
			bg = reduceFunction(finalHash, this.chainLength);
			BigInteger test =checkPossibleSolution(bg,i);
			if(test!=null){
	
				if(HexUtils.toHexString(tripleSha256Encryption(test)).equals(hash)){
					try {
						save(hash + " " + test.toString(16));
					} catch (IOException e) {
						crack(hash);
						e.printStackTrace();
					}
					System.out.println("Found solution for hash : "+ hash +"  password in hexa is : " +  test.toString(16));
					return;
				}
			}
		}
	
	}

	private static byte[] sha256(byte[] input)
	{
		try{
			MessageDigest digest=MessageDigest.getInstance("SHA-256");
			digest.update(input);
			byte[] rslt=digest.digest();

			return rslt;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	private BigInteger computeFinalHash(BigInteger bg, int n) {
		byte[] finalHash = tripleSha256Encryption(bg);
		BigInteger tmp = reduceFunction(finalHash, 0);

		for(int i=1; i<=n;i++){
			finalHash = tripleSha256Encryption(tmp);
			tmp = reduceFunction(finalHash, i);


		}
		return tmp;
	}

	private BigInteger generateRandomNumber(){

		SecureRandom randomGen = new SecureRandom();
		BigInteger bg = new BigInteger(32, randomGen);
		if(bg.bitCount()%2!=1){
			if(bg.bitLength()==0){
				bg = bg.flipBit((Math.abs(randomGen.nextInt())) % 1);
			}else{
				bg = bg.flipBit((Math.abs(randomGen.nextInt()))
						% bg.bitLength());
			}

		}
		return bg;



	}

	private BigInteger checkPossibleSolution(BigInteger bg,int i) {

		for(RainbowTableLine rll: this.rl){
			if(bg.equals(rll.finalReduced)){
				return computeFinalHash(new BigInteger(rll.password,16),i-1);
			}	
		}
		return null;

	}

	private synchronized void save(String string) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(
				CRACKED_FILE_NAME, true));
		writer.write(string);
		writer.newLine();
		writer.close();
		
	}



}