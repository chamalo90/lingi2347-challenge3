package com.jcolmonts.challenge3.model;

import java.math.BigInteger;

/**
 * Object representation of a RainbowTable line which contains only the first
 * password and the final reduced.
 * 
 * @author julien
 * 
 */
public class RainbowTableLine {
	public BigInteger finalReduced;
	public String password;

	public RainbowTableLine(BigInteger hash, String password){
		this.finalReduced = hash;
		this.password = password;
	}
	
	@Override
	public String toString(){
		return password + ':' + finalReduced.toString(16);
	}
}
