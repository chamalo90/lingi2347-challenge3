package com.jcolmonts.challenge3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

import com.jcolmonts.challenge3.model.RainbowTable;
import com.jcolmonts.challenge3.model.RainbowTableLine;

public class Find {

	public static final int LINES = 1000000;

	public static void find(String inputFile){
		System.out.println("---------------------------------------------");
		System.out.println("Starting Rainbow Table Loading ... ... ...");
		RainbowTable rt = new RainbowTable(Challenge3.CHAINLENGTH, Challenge3.CHARSET, Challenge3.LINES_NUMBER);
		try {
			rt.rl = readContent(inputFile);
			rt.lines= rt.rl.size();
		} catch (IOException e) {
			System.out.println("---------------------------------------------");
			System.out.println("Error : Couldn't load Rainbow Table");
			System.out.println("---------------------------------------------");
			e.printStackTrace();
			return;
		}
		System.out.println("---------------------------------------------");
		System.out.println("Rainbow Table Loaded");
		System.out.println("---------------------------------------------");
		System.out.println("Starting Hash File Loading ... ... ...");
		List<String> hashes;
		try {
			hashes = readHashes(inputFile);
		} catch (IOException e) {
			System.out.println("---------------------------------------------");
			System.out.println("Error : Couldn't load Hashes");
			System.out.println("---------------------------------------------");
			e.printStackTrace();
			return;
		}
		for(String hash:hashes){
			System.out.println("Starting search for : " + hash);
			new FindThread(rt,hash).start();;

		}



	}

	private static List<String> readHashes(String inputFile) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(new File(inputFile)));
		List<String> hashes = new LinkedList<String>();
		String line = in.readLine();
		while(line!=null) {
			hashes.add(line);
			line = in.readLine();
		}
		in.close();
		return hashes;
	}

	private static List<RainbowTableLine> readContent(String inputFile) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(new File(
				RainbowTable.HASH_FILE_NAME)));
		List<RainbowTableLine> tmp = new LinkedList<RainbowTableLine>();
		String line;
		do {
			line = in.readLine();
			if (line == null) {
				break;
			}
			int separatorIndex = line.indexOf(':');
			String initialPassword = line.substring(0, separatorIndex);
			String finalHashStr = line.substring(1 + separatorIndex);

			tmp.add(new RainbowTableLine(new BigInteger(finalHashStr, 16),
					initialPassword));
		} while (line != null);
		System.out.println("Loaded : " + tmp.size() + " lines");
		in.close();
		return tmp;
	}




}
