package com.jcolmonts.challenge3;

import com.jcolmonts.challenge3.model.RainbowTable;

/**
 * Thread that will try to crack one hash received as parameter. It will write
 * solution in CRACKED_FILE_NAME if it finds one.
 * 
 * @author julien
 * 
 */
public class FindThread extends Thread{
	public RainbowTable rt;
	public String hash;
    public FindThread(RainbowTable rt, String hash) {
        this.rt=rt;
        this.hash=hash;
    }
    public void run() {
        rt.crack(hash);
    }

}
