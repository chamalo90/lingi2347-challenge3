-----------------------------------------------------------------------------
-   LINGI2347 - Challenge 3 - Time-memory Trade-off                         -
-	author : Julien Colmonts					    -
-	noma : 4163-08-00						    -
-	SINF21MS/G							    -
-----------------------------------------------------------------------------

How to run Challenge 3 ?
- Clone the repository
- In repo directory, using the constraints for this challenge, launch :
		- timeout 900 java -jar Challenge3-Solver.jar 
	then add following arguments to execute the desired computation :
		- precompute : will compute a rainbow table named jcolmonts_rainbow_table of 500000 lines and 8000 of chainlength.
		- find path/to/your/hash_file : will try to crack hashes contained in your hash file and store them into a file named 'cracked_hashes'.

If you want to explore sources, import all files from EclipseProject/src/ in an new eclipse project.
